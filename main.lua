-----------------------------------------------------------------------------------------
--
-- main.lua
-- 本範例示範產生WebView的兩種做法。webView和popWebView。
--
-- webView重點提要:
-- 無法被加入group中，且永遠顯示在一般的顯示元件之上。
-- 支援iOS.Android Device以及Corona Simulator for Mac，不支援Simulator for Windows
--
-- popWebView重點提要:
-- 只支援iOS.Android Device以及Corona Simulator for Mac
-- 一次只能顯示一個popWebView
-- 無法被加入group中，且永遠顯示在一般的顯示元件之上。
-- 無法被移動或更改大小
--
-- Author: Zack Lin
-- Time:2015/4/16
-----------------------------------------------------------------------------------------

--=======================================================================================
--引入各種函式庫
--=======================================================================================
display.setStatusBar( display.HiddenStatusBar )

local widget = require("widget")
--=======================================================================================
--宣告各種變數
--=======================================================================================
_SCREEN = {
	WIDTH = display.viewableContentWidth,
	HEIGHT = display.viewableContentHeight
}
_SCREEN.CENTER = {
	X = display.contentCenterX,
	Y = display.contentCenterY
}

local btn1
local btn2
local webView 
local url = "http://www.gamer.com.tw"

local initial
local handleButtonEvent
local webListener
local hideWebView
local cancelPopupWebView
local popupListener

--=======================================================================================
--宣告與定義main()函式
--=======================================================================================
local main = function (  )
	initial()
end

--=======================================================================================
--定義其他函式
--=======================================================================================
initial = function (  )
	btn1 = widget.newButton{
		id = "btn1",
		width = 220,
	    height = 100,
	    defaultFile = "images/btn_normal.png",
	    overFile = "images/btn_over.png",
	    label = "NewWebView",
	    fontSize = 30,
	    onEvent = handleButtonEvent
	}
	btn1.x = _SCREEN.CENTER.X
	btn1.y = _SCREEN.CENTER.Y + 400

	btn2 = widget.newButton{
		id = "btn2",
		width = 220,
		height = 100,
		defaultFile = "images/btn_normal.png",
	    overFile = "images/btn_over.png",
	    label = "WebPopup",
	    fontSize = 30,
		onEvent = handleButtonEvent
	}
	btn2.x = _SCREEN.CENTER.X
	btn2.y = _SCREEN.CENTER.Y + 300
end

handleButtonEvent = function ( event )
	if ("ended" == event.phase) then
		if ("btn1" == event.target.id) then
			--生成WebView，參數分別為中心點位置以及寬高
			webView = native.newWebView( display.contentCenterX, display.contentCenterY - 100, 640, 650 )
			--對網址進行請求
			webView:request( url )
			--偵聽urlRequest這個事件
			webView:addEventListener( "urlRequest", webListener )
			timer.performWithDelay( 5000, hideWebView, 1 )
		elseif ("btn2" == event.target.id) then
			--如果Options Table裡面有urlRequest，需在listener function已經實作完畢才宣告，否則會產生nil
			local webPopupOptions =
			{
			    baseUrl = nil, -- default: nil，一般用於請求本地html檔
			    hasBackground = false,  -- default: true
			    autoCancel = true,  -- default: true (Android)，設定是否當Android裝置按下Back鍵時關閉PopupWebView
			    urlRequest = popupListener  -- default: nil，設定請求偵聽器
			}
			--開啟popupWebView
			native.showWebPopup( url, webPopupOptions )
			timer.performWithDelay( 15000, cancelPopupWebView, 1 )
		end
	end
end

--webView偵聽器
webListener = function ( event )
	print( 'webListener' )
	if event.url then
		print( "Info" ,  "The event.url is : " .. event.url )
		native.showAlert("Info" ,  "The event.url is : " .. event.url , { "OK" });
    end

    if event.type then
    	native.showAlert("Info" ,  "The event.type is : " .. event.type , { "OK" });
    end

    if event.errorCode then
        native.showAlert( "Error!", event.errorMessage, { "OK" } )
    end
end

--popWebView偵聽器，當shouldLoad return false的時候，將不執行該次網頁請求而自動關閉popupWebView
popupListener = function ( event )
    local shouldLoad = true
    print( "Info" ,  "The event.url is : " .. event.url )
    local url = event.url
    if 1 == string.find( url, "corona:close" ) then
        -- Close the web popup
        shouldLoad = false
    end

    if event.errorCode then
        -- Error loading page
        print( "Error: " .. tostring( event.errorMessage ) )
        shouldLoad = false
    end

    return shouldLoad
end

--移除webView
hideWebView = function (  )
	webView:removeEventListener( "urlRequest", webListener)
	--移除webView需使用removeSelf方法
	webView:removeSelf()
	webView = nil

end

--移除popWebView
cancelPopupWebView = function (  )
	native.cancelWebPopup( )
end



--=======================================================================================
--呼叫主函式
--=======================================================================================
main()
